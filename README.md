AGAPT

steps to install in new ubuntu 14.04 server

* sudo apt-get update -y
* sudo apt-get upgrade -y
* sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
* echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
* sudo apt-get update
* sudo  apt-get install -y mongodb-org
* sudo  apt-get install nodejs-legacy
* sudo apt-get install npm
* sudo apt-get install git
* sudo npm install -g bower
* sudo npm install -g grunt-cli
* git clone https://sattree@bitbucket.org/agapt/agapt.git agapt (if you use sudo here bower update will throw error)
* bower update
* sudo npm install

Node server - API calls
host/orders(GET) - gets all the existing orders in json format host/orders(POST - creates new order