'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(['$rootScope', '$location', 'Authentication', 
	function ($rootScope, $location, Authentication) {
		$rootScope.$on('$locationChangeStart', function (event) {
console.log('the route', $location.url(), Authentication.user);
	        if (!Authentication.user && $location.url() !== '/login') {
	          console.log('DENY : Redirecting to Login');
	          event.preventDefault();
	          //$state.go('login');
	          $location.path('/login');
	        }
	        else {
	          console.log('ALLOW');
	        }
		});
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});