'use strict';

(function() {
	// Uploads Controller Spec
	describe('Uploads Controller Tests', function() {
		// Initialize global variables
		var UploadsController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Uploads controller.
			UploadsController = $controller('UploadsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one upload object fetched from XHR', inject(function(Uploads) {
			// Create sample upload using the Uploads service
			var sampleUpload = new Uploads({
				title: 'An Upload about MEAN',
				content: 'MEAN rocks!'
			});

			// Create a sample uploads array that includes the new upload
			var sampleUploads = [sampleUpload];

			// Set GET response
			$httpBackend.expectGET('uploads').respond(sampleUploads);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.uploads).toEqualData(sampleUploads);
		}));

		it('$scope.findOne() should create an array with one upload object fetched from XHR using a uploadId URL parameter', inject(function(Uploads) {
			// Define a sample upload object
			var sampleUpload = new Uploads({
				title: 'An Upload about MEAN',
				content: 'MEAN rocks!'
			});

			// Set the URL parameter
			$stateParams.uploadId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/uploads\/([0-9a-fA-F]{24})$/).respond(sampleUpload);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.upload).toEqualData(sampleUpload);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Uploads) {
			// Create a sample upload object
			var sampleUploadPostData = new Uploads({
				title: 'An Upload about MEAN',
				content: 'MEAN rocks!'
			});

			// Create a sample upload response
			var sampleUploadResponse = new Uploads({
				_id: '525cf20451979dea2c000001',
				title: 'An Upload about MEAN',
				content: 'MEAN rocks!'
			});

			// Fixture mock form input values
			scope.title = 'An Upload about MEAN';
			scope.content = 'MEAN rocks!';

			// Set POST response
			$httpBackend.expectPOST('uploads', sampleUploadPostData).respond(sampleUploadResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.title).toEqual('');
			expect(scope.content).toEqual('');

			// Test URL redirection after the upload was created
			expect($location.path()).toBe('/uploads/' + sampleUploadResponse._id);
		}));

		it('$scope.update() should update a valid upload', inject(function(Uploads) {
			// Define a sample upload put data
			var sampleUploadPutData = new Uploads({
				_id: '525cf20451979dea2c000001',
				title: 'An Upload about MEAN',
				content: 'MEAN Rocks!'
			});

			// Mock upload in scope
			scope.upload = sampleUploadPutData;

			// Set PUT response
			$httpBackend.expectPUT(/uploads\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/uploads/' + sampleUploadPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid uploadId and remove the upload from the scope', inject(function(Uploads) {
			// Create new upload object
			var sampleUpload = new Uploads({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new uploads array and include the upload
			scope.uploads = [sampleUpload];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/uploads\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleUpload);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.uploads.length).toBe(0);
		}));
	});
}());