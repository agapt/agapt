'use strict';

//Consumes service used for communicating with the consumes REST endpoints
angular.module('consumes').factory('Consumes', ['$resource',
	function($resource) {
		return {
			consumes: $resource('consumes/:consumeId', {
					consumeId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			}),
			consumers: $resource('consumers/:consumeId', {
					consumeId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			})
		};
	}
]);