'use strict';

(function() {
	// Consumes Controller Spec
	describe('Consumes Controller Tests', function() {
		// Initialize global variables
		var ConsumesController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Consumes controller.
			ConsumesController = $controller('ConsumesController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one consume object fetched from XHR', inject(function(Consumes) {
			// Create sample consume using the Consumes service
			var sampleConsume = new Consumes({
				title: 'An Consume about MEAN',
				content: 'MEAN rocks!'
			});

			// Create a sample consumes array that includes the new consume
			var sampleConsumes = [sampleConsume];

			// Set GET response
			$httpBackend.expectGET('consumes').respond(sampleConsumes);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.consumes).toEqualData(sampleConsumes);
		}));

		it('$scope.findOne() should create an array with one consume object fetched from XHR using a consumeId URL parameter', inject(function(Consumes) {
			// Define a sample consume object
			var sampleConsume = new Consumes({
				title: 'An Consume about MEAN',
				content: 'MEAN rocks!'
			});

			// Set the URL parameter
			$stateParams.consumeId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/consumes\/([0-9a-fA-F]{24})$/).respond(sampleConsume);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.consume).toEqualData(sampleConsume);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Consumes) {
			// Create a sample consume object
			var sampleConsumePostData = new Consumes({
				title: 'An Consume about MEAN',
				content: 'MEAN rocks!'
			});

			// Create a sample consume response
			var sampleConsumeResponse = new Consumes({
				_id: '525cf20451979dea2c000001',
				title: 'An Consume about MEAN',
				content: 'MEAN rocks!'
			});

			// Fixture mock form input values
			scope.title = 'An Consume about MEAN';
			scope.content = 'MEAN rocks!';

			// Set POST response
			$httpBackend.expectPOST('consumes', sampleConsumePostData).respond(sampleConsumeResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.title).toEqual('');
			expect(scope.content).toEqual('');

			// Test URL redirection after the consume was created
			expect($location.path()).toBe('/consumes/' + sampleConsumeResponse._id);
		}));

		it('$scope.update() should update a valid consume', inject(function(Consumes) {
			// Define a sample consume put data
			var sampleConsumePutData = new Consumes({
				_id: '525cf20451979dea2c000001',
				title: 'An Consume about MEAN',
				content: 'MEAN Rocks!'
			});

			// Mock consume in scope
			scope.consume = sampleConsumePutData;

			// Set PUT response
			$httpBackend.expectPUT(/consumes\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/consumes/' + sampleConsumePutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid consumeId and remove the consume from the scope', inject(function(Consumes) {
			// Create new consume object
			var sampleConsume = new Consumes({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new consumes array and include the consume
			scope.consumes = [sampleConsume];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/consumes\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleConsume);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.consumes.length).toBe(0);
		}));
	});
}());