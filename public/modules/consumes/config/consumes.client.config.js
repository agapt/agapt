'use strict';

// Configuring the Consumes module
angular.module('consumes').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Consumes', 'consumes', 'dropdown', '/consumes(/create)?');
		Menus.addSubMenuItem('topbar', 'consumes', 'List Consumes', 'consumes');
		Menus.addSubMenuItem('topbar', 'consumes', 'New Consume', 'consumes/create');
	}
]);