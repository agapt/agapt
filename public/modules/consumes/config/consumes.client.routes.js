'use strict';

// Setting up route
angular.module('consumes').config(['$stateProvider',
	function($stateProvider) {
		// Consumes state routing
		$stateProvider.
		state('listConsumes', {
			url: '/consumes',
			templateUrl: 'modules/consumes/views/list-consumes.client.view.html'
		}).
		state('createConsume', {
			url: '/consumes/create',
			templateUrl: 'modules/consumes/views/create-consume.client.view.html'
		}).
		state('viewConsume', {
			url: '/consumes/:consumeId',
			templateUrl: 'modules/consumes/views/view-consume.client.view.html'
		}).
		state('editConsume', {
			url: '/consumes/:consumeId/edit',
			templateUrl: 'modules/consumes/views/edit-consume.client.view.html'
		});
	}
]);