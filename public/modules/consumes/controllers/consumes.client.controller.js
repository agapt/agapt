'use strict';

// Consumes controller
angular.module('consumes').controller('ConsumesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Consumes',
	function($scope, $stateParams, $location, Authentication, Consumes) {
		$scope.authentication = Authentication;

		// Create new Consume
		$scope.create = function() {
			// Create new Consume object
			var consume = new Consumes({
				title: this.title,
				summary: this.summary,
				image: this.image,
				description: this.description,
				bullet_point: this.bullet_point,
				type: this.type,
				id: this.id,
				offered_by: this.offered_by,
				category: this.category,
				units: this.units,
				price: this.price,
				keywords: this.keywords
			});

			// Redirect after save
			consume.$save(function(response) {
				$location.path('consumes/' + response._id);

				// Clear form fields
				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Consume
		$scope.remove = function(consume) {
			if (consume) {
				consume.$remove();

				for (var i in $scope.consumes) {
					if ($scope.consumes[i] === consume) {
						$scope.consumes.splice(i, 1);
					}
				}
			} else {
				$scope.consume.$remove(function() {
					$location.path('consumes');
				});
			}
		};

		// Update existing Consume
		$scope.update = function() {
			var consume = $scope.consume;

			consume.$update(function() {
				$location.path('consumes/' + consume._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Consumes
		$scope.find = function() {
			$scope.consumes = Consumes.query();
		};

		// Find existing Consume
		$scope.findOne = function() {
			$scope.consume = Consumes.get({
				consumeId: $stateParams.consumeId
			});
		};
	}
]);