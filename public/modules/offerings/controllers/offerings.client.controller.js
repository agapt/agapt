'use strict';

// Offerings controller
angular.module('offerings').controller('OfferingController', ['$scope', '$state', '$stateParams', 
			'$location', 'Authentication', 'Offerings', 'FileUploader',  'Uploads', 
	function($scope, $state, $stateParams, $location, Authentication, Offerings, FileUploader, Uploads) {
		$scope.authentication = Authentication;

		$scope.init = function(){
			var itemId = $stateParams.itemId;
			if(itemId){
				$scope.findOne();
				$scope.getUploads(itemId);
			} else {
				$scope.offering = new Offerings();
				$scope.offering.keywords = [];
				$scope.offering.bullets = [];
				$scope.offering.type = 'product';
			}

			var offeringId = itemId || $scope.offering._id;
			$scope.uploader = new FileUploader({
	        	url: 'uploads',
	        	formData: [{'offeringId': offeringId}]
	        });
	        $scope.uploader.onCompleteAll = function(fileItem, response, status, headers) {
	            console.info('onSuccessItem', fileItem, response, status, headers);
	            $scope.getUploads(offeringId);
	        };
		}

		// Create new Offering
		$scope.create = function() {
			if($scope.offering._id){
				$scope.update();
				return;
			}
			// Create new Offering object
			var offering = $scope.offering;

			// Redirect after save
			offering.$save(function(response) {
				//$state.go('dashboards.home');
				$scope.uploader.formData = [{'offeringId': $scope.offering._id}];
				//$scope.getUploads($scope.offering._id);
				$scope.success = true;
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.viewItem = function(id){
	        $state.go('app.page', { module: 'offerings', page: 'view-offering', child: null, itemId : id});
	        console.log('view the item');
	    };

	    $scope.editItem = function(id){
	        $state.go('app.page', { module: 'offerings', page: 'create-offering', child: null, itemId : id});
	    };


		// Remove existing Offering
		$scope.remove = function(offering) {
			if (offering) {
				offering.$remove();

				for (var i in $scope.offerings) {
					if ($scope.offerings[i] === offering) {
						$scope.offerings.splice(i, 1);
					}
				}
			} else {
				$scope.offering.$remove(function() {
					$location.path('offerings');
				});
			}
		};

		// Update existing Offering
		$scope.update = function() {
			var offering = $scope.offering;

			offering.$update(function() {
				//$location.path('offerings/' + offering._id);
				$scope.success = true;
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Offerings
		$scope.find = function() {
			$scope.offerings = Offerings.query();
		};

		// Find existing Offering
		$scope.findOne = function() {
			console.log('the id is the', $stateParams.itemId);
			$scope.offering = Offerings.get({
				offeringId: $stateParams.itemId
			});
		};

		$scope.getUploads = function(itemId){
			$scope.uploads = Uploads.get({
				uploadId: itemId
			});
		}
	}
]);