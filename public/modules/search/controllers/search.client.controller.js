'use strict';

// Searchs controller
angular.module('search').controller('SearchController', ['$scope', '$state', '$stateParams', '$location', 'Authentication', 'Search',
	 'Fetch', 'Consumes', 'Entities', 
	function($scope, $state, $stateParams, $location, Authentication, Search, Fetch, Consumes, Entities) {
		$scope.authentication = Authentication;
		$scope.val = '';
		$scope.source = 'Item';
		$scope.typeLabel = 'Products';
		$scope.items = [];
		var inputParam = $stateParams.input;

		$scope.search = function(){
			//$state.go('viewSearch', {'input': $scope.val});
			//$location.path('searchs/' + $scope.val);
			console.log('the val here is: ', $scope.val);
			console.log('the input patram here is: ', $scope.authentication.user);
			$scope.getItems($scope.val);

		};

		$scope.getItems = function(val){
			Fetch.conditioned({'query': val, source: $scope.source}, function(items){
				console.log('the items: ', items);
				$scope.items = items;
      		});
		};

		$scope.create = function(itemId){
			// Create new Article object
			var consume = new Consumes.consumes({
				item: itemId
			});

			// Redirect after save
			consume.$save(function(response) {
				$state.go('dashboards.home');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});		
		};

		$scope.connect = function(toId){
			var entity = $scope.entity;
			var connection = new Entities.connections({
				from : '',
				to : toId
			});

			connection.$save(function(response) {
				$state.reload();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		if(inputParam){
			$scope.val = inputParam;
			$scope.getItems(inputParam);
		}

		$scope.setFilterField = function(source, label){
			$scope.source = source;
			$scope.typeLabel = label;
		};
	}
]);