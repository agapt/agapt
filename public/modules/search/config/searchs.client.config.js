'use strict';

// Configuring the Searchs module
angular.module('search').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Searchs', 'searchs', 'dropdown', '/searchs(/create)?');
		Menus.addSubMenuItem('topbar', 'searchs', 'List Searchs', 'searchs');
		Menus.addSubMenuItem('topbar', 'searchs', 'New Search', 'searchs/create');
	}
]);