'use strict';

// Setting up route
angular.module('search').config(['$stateProvider',
	function($stateProvider) {
		// Searchs state routing
		$stateProvider.
		state('createSearch', {
			url: '/search/create',
			templateUrl: 'modules/search/views/create-search.client.view.html'
		}).
		state('viewSearch', {
			url: '/search/:input',
			templateUrl: 'modules/search/views/view-search.client.view.html'
		});
	}
]);