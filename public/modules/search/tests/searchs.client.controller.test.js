'use strict';

(function() {
	// Searchs Controller Spec
	describe('Searchs Controller Tests', function() {
		// Initialize global variables
		var SearchsController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Searchs controller.
			SearchsController = $controller('SearchsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one search object fetched from XHR', inject(function(Searchs) {
			// Create sample search using the Searchs service
			var sampleSearch = new Searchs({
				title: 'An Search about MEAN',
				content: 'MEAN rocks!'
			});

			// Create a sample searchs array that includes the new search
			var sampleSearchs = [sampleSearch];

			// Set GET response
			$httpBackend.expectGET('searchs').respond(sampleSearchs);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.searchs).toEqualData(sampleSearchs);
		}));

		it('$scope.findOne() should create an array with one search object fetched from XHR using a searchId URL parameter', inject(function(Searchs) {
			// Define a sample search object
			var sampleSearch = new Searchs({
				title: 'An Search about MEAN',
				content: 'MEAN rocks!'
			});

			// Set the URL parameter
			$stateParams.searchId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/searchs\/([0-9a-fA-F]{24})$/).respond(sampleSearch);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.search).toEqualData(sampleSearch);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Searchs) {
			// Create a sample search object
			var sampleSearchPostData = new Searchs({
				title: 'An Search about MEAN',
				content: 'MEAN rocks!'
			});

			// Create a sample search response
			var sampleSearchResponse = new Searchs({
				_id: '525cf20451979dea2c000001',
				title: 'An Search about MEAN',
				content: 'MEAN rocks!'
			});

			// Fixture mock form input values
			scope.title = 'An Search about MEAN';
			scope.content = 'MEAN rocks!';

			// Set POST response
			$httpBackend.expectPOST('searchs', sampleSearchPostData).respond(sampleSearchResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.title).toEqual('');
			expect(scope.content).toEqual('');

			// Test URL redirection after the search was created
			expect($location.path()).toBe('/searchs/' + sampleSearchResponse._id);
		}));

		it('$scope.update() should update a valid search', inject(function(Searchs) {
			// Define a sample search put data
			var sampleSearchPutData = new Searchs({
				_id: '525cf20451979dea2c000001',
				title: 'An Search about MEAN',
				content: 'MEAN Rocks!'
			});

			// Mock search in scope
			scope.search = sampleSearchPutData;

			// Set PUT response
			$httpBackend.expectPUT(/searchs\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/searchs/' + sampleSearchPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid searchId and remove the search from the scope', inject(function(Searchs) {
			// Create new search object
			var sampleSearch = new Searchs({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new searchs array and include the search
			scope.searchs = [sampleSearch];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/searchs\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleSearch);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.searchs.length).toBe(0);
		}));
	});
}());