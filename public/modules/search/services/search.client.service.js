'use strict';

//Searchs service used for communicating with the searchs REST endpoints
angular.module('search').factory('Search', ['$resource',
	function($resource) {
		return $resource('search/:searchId', {
			searchId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Fetch', ['$resource',
  function($resource) {
    return $resource('elastic-search', {},{
          conditioned: {method:'GET', params:{}, isArray:true}
      });
  }]);