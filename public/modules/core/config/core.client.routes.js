'use strict';

var coreConfigs = angular.module('core', []);

coreConfigs.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

    // For any unmatched url, send to /dashboard
    $urlRouterProvider.otherwise("/app/core/dashboard/");

    $stateProvider
        .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: 'modules/core/views/home.client.view.html'
        })

        // loading page templates dynamically for demo
        .state('app.page', {
            url: '/:module/:page/:child',
            params: {
                module: { value: null},
                page: { value: null },
                child: { value: null },
                itemId: { value: null }
            },
            resolve: {
                deps: ['scriptLoader', function(scriptLoader){
                    return scriptLoader;
                }]
            },
            templateProvider: function ($http, $stateParams, scriptLoader) {
                console.log('the params: ', $stateParams.itemId);
                return $http.get('modules/' + $stateParams.module + '/views/' + $stateParams.page + ( /*optional param*/ $stateParams.child ? '_' + $stateParams.child : '') + '.client.view.html')
                    .then(function(response) {
                        return scriptLoader.loadScriptTagsFromData(response.data);
                    })
                    .then(function(responseData){
                        return responseData;
                    });
            }
        })

        //separate state for login & error pages
        .state('login', {
            url: '/login',
            templateUrl: 'modules/users/views/authentication/login.client.view.html'
        })
        .state('error', {
            url: '/error',
            templateUrl: 'modules/dashboards/views/error.html'
        })
}]);

coreConfigs.value('uiJqDependencies', {
    'mapael': [
        'lib/raphael/raphael-min.js',
        'lib/jQuery-Mapael/js/jquery.mapael.js'
    ],
    'easyPieChart': [
        'lib/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
    ],
    'autosize': [
        'lib/jquery-autosize/jquery.autosize.min.js'
    ],
    'wysihtml5': [
        'lib/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.min.js',
        'lib/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js'
    ],
    'select2': [
        'lib/select2/select2.min.js'
    ],
    'markdown': [
        'lib/markdown/lib/markdown.js',
        'lib/bootstrap-markdown/js/bootstrap-markdown.js'
    ],
    'datetimepicker': [
        'lib/moment/min/moment.min.js',
        'lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
    ],
    'colorpicker': [
        'lib/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js'
    ],
    'inputmask': [
        'lib/jasny-bootstrap/js/inputmask.js'
    ],
    'fileinput': [
        'lib/holderjs/holder.js',
        'lib/jasny-bootstrap/js/fileinput.js'
    ],
    'slider': [
        'lib/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js'
    ],
    'parsley': [
        'lib/parsleyjs/dist/parsley.min.js'
    ],
    'sortable': [
        'lib/jquery-ui/ui/core.js',
        'lib/jquery-ui/ui/widget.js',
        'lib/jquery-ui/ui/mouse.js',
        'lib/jquery-ui/ui/sortable.js',
        'lib/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'
    ],
    'draggable': [
        'lib/jquery-ui/ui/core.js',
        'lib/jquery-ui/ui/widget.js',
        'lib/jquery-ui/ui/mouse.js',
        'lib/jquery-ui/ui/draggable.js'
    ],
    'nestable': [
        'lib/jquery.nestable/jquery.nestable.js'
    ],
    'vectorMap': [
        'lib/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'lib/jvectormap-world/index.js'
    ],
    'sparkline': [
        'lib/jquery.sparkline/index.js'
    ],
    'magnificPopup': [
        'lib/magnific-popup/dist/jquery.magnific-popup.min.js'
    ]
});