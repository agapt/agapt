'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$window', function($window) {
	var auth = {
		user: $window.user,
		isLoggedIn : function(){
          return($window.user)? true : false;
        }
	};
	
	return auth;
}]).factory('LoginAs', ['$resource',
	function($resource) {
		return $resource('loginas/:userId', {
			userId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
