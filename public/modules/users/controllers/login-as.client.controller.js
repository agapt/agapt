'use strict';

angular.module('users').controller('LoginAsController', ['$scope', '$state', '$http', '$location', 'Authentication', 'LoginAs',
	function($scope, $state, $http, $location, Authentication, LoginAs) {
		$scope.authentication = Authentication;

		$scope.find = function() {
			$scope.users2 = LoginAs.query();
		};

		$scope.signin = function(uname, pass) {
			var cred = {username: uname, password: pass};
			$http.post('/auth/signin', cred).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response.user;
				var loginas = new LoginAs({
					user: $scope.authentication.user._id,
					password: pass
				});

				// Redirect after save
				loginas.$save();
				$location.path('/app/dashboard/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);