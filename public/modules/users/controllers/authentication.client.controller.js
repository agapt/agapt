'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', '$state', 'Authentication',
	function($scope, $http, $location, $state, Authentication) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response.user;

				// And redirect to the index page
				//$state.go("app.page({ page: 'dashboard', child: null })");
				$location.path('/app/dashboard/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);