'use strict';

// Entities controller
angular.module('entities').controller('EntitiesController', ['$scope', '$state', '$stateParams', '$location', 'Authentication', 'Entities',
	function($scope, $state, $stateParams, $location, Authentication, Entities) {
		$scope.authentication = Authentication;

		// Create new Entity
		$scope.create = function() {
			// Create new Entity object
			var entity = new Entities({
				title: this.title,
				content: this.content
			});

			// Redirect after save
			entity.$save(function(response) {
				$location.path('entities/' + response._id);

				// Clear form fields
				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Entity
		$scope.remove = function(entity) {
			if (entity) {
				entity.$remove();

				for (var i in $scope.entities) {
					if ($scope.entities[i] === entity) {
						$scope.entities.splice(i, 1);
					}
				}
			} else {
				$scope.entity.$remove(function() {
					$location.path('entities');
				});
			}
		};

		// Update existing Entity
		$scope.update = function() {
			var entity = $scope.entity;

			entity.$update(function() {
				$location.path('entities/' + entity._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Update existing Article
		$scope.connUpdate = function(connId) {
			var connection = new Entities.pendingConnections({
				_id : connId
			});
			//var connection = $scope.pendingConnections[0];

			connection.$update(function(response) {
				$state.reload();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Update existing Article
		$scope.connectionRemove = function(connId) {
			var connection = new Entities.connections({
				_id : connId
			});
			//var connection = $scope.pendingConnections[0];

			connection.$remove(function(response) {
				$state.reload();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.connect = function(toId){
			var entity = $scope.entity;
			var connection = new Entities.connections({
				from : entity._id,
				to : toId
			});

			connection.$save(function(response) {
				$state.reload();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.init = function(){
			$scope.findOne();
			$scope.find();
			$scope.getSuggestConnections();
			$scope.getConnections();
			$scope.getPendingConnections();
			$scope.getRequestedConnections();
		};

		// Find a list of Entities
		$scope.find = function() {
			$scope.entities = Entities.entities.query();
		};

		// Find existing Entity
		$scope.findOne = function() {
			$scope.entity = Entities.entities.get({
				entityId: $stateParams.entityId
			});
			//$scope.connections = $scope.getConnections();
		};

		$scope.getSuggestConnections = function(){
			$scope.suggestConnections = Entities.suggestConnections.query({entityId: $stateParams.entityId});
		};

		$scope.getConnections = function(){
			$scope.connections = Entities.connections.query({entityId: $stateParams.entityId});
		};

		$scope.getPendingConnections = function(){
			$scope.pendingConnections = Entities.pendingConnections.query({entityId: $stateParams.entityId});
		};

		$scope.getRequestedConnections = function(){
			$scope.requestedConnections = Entities.requestedConnections.query({entityId: $stateParams.entityId});
		};
	}
]);