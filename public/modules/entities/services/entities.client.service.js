'use strict';

//Entities service used for communicating with the entities REST endpoints
angular.module('entities').factory('Entities', ['$resource',
	function($resource) {
		return {
			entities: $resource('entities/:entityId', {
					entityId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			}),
			connections: $resource('connections/:connId', {
					connId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			}),
			suggestConnections: $resource('connections/suggest', {
					connId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			}),
			pendingConnections: $resource('connections/pending/:connId', {
					connId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			}),
			requestedConnections: $resource('connections/requested', {
					connId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			}),
			messages: $resource('messages', {
					connId: '@_id'
				}, {
				update: {
					method: 'PUT'
				}
			})
		};
	}
]);