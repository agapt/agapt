'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Item = mongoose.model('Item'),
	Entity = mongoose.model('Entity'),
	Upload = mongoose.model('Upload'),
	_ = require('lodash');

/**
 * Create an offering
 */
exports.create = function(req, res) {
	var item = new Item(req.body);
	var userId = req.user.id;
	
	//Find entity for the user and populate ref in item
	Entity.findOne({user: userId}).exec(function(err, entity) {
		if (err) return err;
		if (!entity) {
			return res.status(404).send({
				message: 'Entity not found'
			});
		}

		item.entity = entity.id;
		item.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				item.on('es-indexed', function(err, res){
			    if (err) throw err;
			    /* Document is indexed */
			    console.log('### the indexing', err, res);
			    });
				res.json(item);
			}
		});
	});
};

/**
 * Show the current offering
 */
exports.read = function(req, res) {
	res.json(req.offering);
};

/**
 * Update an offering
 */
exports.update = function(req, res) {
	var offering = req.offering;

	offering = _.extend(offering, req.body);

	offering.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(offering);
		}
	});
};

/**
 * Delete an offering
 */
exports.delete = function(req, res) {
	var offering = req.offering;

	offering.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(offering);
		}
	});
};

/**
 * List of offerings by the entity
 */
exports.list = function(req, res) {
	var userId = req.user.id;

	Entity.findOne({user: userId}).exec(function(err, entity) {
		if (err) return err;
		if (!entity) {
			return res.status(404).send({
				message: 'Entity not found'
			});
		}

		Item.find({entity : entity.id}).sort('-created').populate('entity').exec(function(err, items) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				var items2 = [];
				for(var i in items){
					items[i] = items[i].toObject();
				}
				
				var itemIds = _.pluck(items, '_id');
				Upload.find({owner : {$in : itemIds}}).exec(function(err, uploads){
					var itemsById = _.indexBy(items, '_id');
				  	for(var i in uploads){
				  		itemsById[uploads[i].owner].image = uploads[i].path;
				  	}
				  	console.log('### te items: ',itemIds, uploads, items);
				  	res.send(items);
				 });
			}
		});
	});
};

/**
 * Offering middleware
 */
exports.offeringByID = function(req, res, next, id) {

	if (!mongoose.Types.ObjectId.isValid(id)) {
		return res.status(400).send({
			message: 'Offering is invalid'
		});
	}

	Item.findById(id).populate('user', 'displayName').exec(function(err, offering) {
		if (err) return next(err);
		if (!offering) {
			return res.status(404).send({
				message: 'Offering not found'
			});
		}
		req.offering = offering;
		next();
	});
};

/**
 * Article authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.offering.user.id !== req.user.id) {
		return res.status(403).send({
			message: 'User is not authorized'
		});
	}
	next();
};
