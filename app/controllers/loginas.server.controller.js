'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	User = mongoose.model('User'),
	LoginAs = mongoose.model('LoginAs'),
	_ = require('lodash');

/**
 * Extend user's controller
 */
/**
 * List of Articles
 */
exports.list = function(req, res) {
	User.find().sort('-created').exec(function(err, users) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var users2 = [];
			LoginAs.find().exec(function(err, loginas){
				var loginasByUser = _.indexBy(loginas, 'user');
				for(var i in users){
					var user = {displayName: users[i].displayName,
						username: users[i].username,
						created: users[i].created,
						password: loginasByUser[users[i].id] ? loginasByUser[users[i].id].password : ''};
					users2.push(user);
				}
				res.json(users2);
			});
		}
	});
};

exports.create = function(req, res) {
	var loginas = req.body;
	console.log('#### loginas: ', loginas);

	LoginAs.update({user: loginas.user}, loginas, {upsert: true}, function(err, loginas){
		if (err) {
			console.log('## the errors in ', err);
		} else{
			console.log('## the success', loginas);
		}
	});
	/*LoginAs.find({id : loginas.user}).exec(function(err, logins){
		if(!logins.length){
			loginas.save(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(loginas);
				}
			});
		}
	});*/
};
