'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
  	User = mongoose.model('User'),
  	Entity = mongoose.model('Entity'),
  	Upload = mongoose.model('Upload'),
  	Connection = mongoose.model('Connection'),
	_ = require('lodash');

/**
 * Search the system by source object
 */
exports.search = function(req, res) {
	var userId = req.user.id;
	var Source = mongoose.model(req.query.source);
	Entity.findOne({user: userId}).exec(function(err, entity) {
		if (err) return err;
		if (!entity) {
			return res.status(404).send({
				message: 'Entity not found'
			});
		}

		Source.search({ query: req.query.query}, function (err, results) {
		  results = results ? results.hits : [];
		  var items = [];
		  for(var i in results){
		  	console.log('the enitity id is: ', results[i]._source._id, userId);
		  	var entityId = req.query.source !== 'User' ? results[i]._source.entity : results[i]._source._id;
		  	var entityId2 = req.query.source !== 'User' ? entity.id : userId;
		  	if(entityId === entityId2){
		  		continue;
		  	}
		  	items.push(results[i]);
		  }
		  var itemIds = _.pluck(items, '_id');
		  Upload.find({owner : {$in : itemIds}}).exec(function(err, uploads){
		  	var itemsById = _.indexBy(items, '_id');
		  	for(var i in uploads){
		  		itemsById[uploads[i].owner]._source.image = uploads[i].path;
		  	}

			Connection.find({$and: [{connected: true}, {$or: [{from: entity.id}, {to: entity.id}]}]}).sort('-created').exec(function(err, connections) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					var itemsByOwnerId = _.indexBy(items, function(item){
						return item._source.entity;
					});

					for(var i in connections){
						//=== not working, why??
						var connectedEntityId = connections[i].from == entity.id ? connections[i].to : connections[i].from;
						var connectedItem = itemsByOwnerId[connectedEntityId];
						if(connectedItem){
							connectedItem._source.connected = true;
						}
						console.log('### the connections: ', items[0]._source, connectedItem, connectedEntityId, connections[i].from === entity.id);
					}

					res.send(items);
				}
			});
		  });
		});
	});
};
