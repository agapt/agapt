'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Entity = mongoose.model('Entity'),
	Consume = mongoose.model('Consume'),
	_ = require('lodash');

/**
 * List of consumed items by the entity
 */
exports.consumed = function(req, res) {
	var entityId = req.query.entityId;

	Consume.find({entity: entityId}).populate('item', 'name').sort('-created').exec(function(err, consumes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(consumes);
		}
	});
};

/**
 * List of consumers across the board
 */
exports.consumers = function(req, res) {
	var entityId = req.query.entityId;

	Consume.find().sort('-created').deepPopulate('entity.user.displayName').exec(function(err, consumers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(consumers);
		}
	});
};

/**
 * Create a new consume request
 */
exports.create = function(req, res) {
	var consume = new Consume(req.body);
	consume.user = req.user;

	var userId = req.user.id;
	
	//Find entity for the user and set as ref in consume request
	Entity.findOne({user: userId}).exec(function(err, entity) {
		if (err) return err;
		if (!entity) {
			return res.status(404).send({
				message: 'Entity not found'
			});
		}

		consume.entity = entity.id;

		consume.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(consume);
			}
		});
	});
};