'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	_ = require('lodash');

exports.create = function(req, res) {
  var source = req.query.source;
  var data = req.body;
  var x = [];
  for( var i in data ) {
      x[i] = data[i];
  }
  console.log('the data is: ', x);
  console.log('the source is: ', source);

  var SourceObject = mongoose.model(source);
  SourceObject.collection.insert(x, function(err, data) {
    if (err) {
      console.log('the error: ', err);
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
            console.log('the success: ', data);

      res.json({'inserted': data});
    }
  });
};
