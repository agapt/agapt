'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	User = mongoose.model('User'),
	Connection = mongoose.model('Connection'),
	Entity = mongoose.model('Entity'),
	_ = require('lodash');

/**
 * Show the current article
 */
exports.read = function(req, res) {
	res.json(req.user);
};

/**
 * Create a article
 */
exports.connect = function(req, res) {
	var userId = req.user.id;
	Entity.findOne({user: userId}).exec(function(err, entity) {
		if (err) return err;
		if (!entity) {
			return res.status(404).send({
				message: 'Entity not found'
			});
		}

		var connection = new Connection({from: entity.id, to: req.body.to});
		connection.save(function(err) {
			if (err) {
				console.log('### the error: ', err, entity.id, req.body);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(connection);
			}
		});
	});
};

/**
 * Update a article
 */
exports.update = function(req, res) {
	var connection = req.connection;
	connection = _.extend(connection, req.body);
	Connection.update({_id: connection._id}, {connected: true}, function(err, connection) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(connection);
		}
	});
};

/**
 * Delete an article
 */
exports.deleteConnection = function(req, res) {
	var connId = req.params.connId;

	Connection.remove({_id: connId}, function(err, connection) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(connection);
		}
	});
};

/**
 * List of Articles
 */
exports.list = function(req, res) {
	Entity.find({'user' : {'$ne': null}}).sort('-created').populate('user', 'displayName').exec(function(err, entities) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(entities);
		}
	});
};

/**
 * List of Articles
 */
exports.suggestConnections = function(req, res) {
	var entityId = req.query.entityId;

	Connection.find({$or: [{from: entityId}, {to: entityId}]}).sort('-created').exec(function(err, users) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var userIds = _.map(users, function(user){
				return (user.from === entityId ? user.to : user.from);
			});
			userIds.push(entityId);
			User.find({_id : {$nin : userIds}}).sort('-created').exec(function(err, users) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(users);
				}
			});
			//res.json(users);
		}
	});
};

/**
 * List of Articles
 */
exports.listConnections = function(req, res) {
	//cannot be the user, entity has to be passed, cud be viewing any entity
	var entity = req.user;
	//entity.id = new ObjectId(entity.id);
	var entityId = req.query.entityId;
//{$and: [{connected: true}, {$or: [{from: entityId}, {to: entityId}]}]}
	Connection.find({$and: [{connected: true}, {$or: [{from: entityId}, {to: entityId}]}]}).sort('-created').populate('from', 'displayName').populate('to', 'displayName').exec(function(err, users) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(users);
		}
	});
};

/**
 * List of Articles
 */
exports.pendingConnections = function(req, res) {
	var entity = req.user;
	var entityId = req.query.entityId;
	Connection.find({$and: [{connected: false}, {to: entityId}]}).sort('-created').populate('from', 'displayName').populate('to', 'displayName').exec(function(err, users) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(users);
		}
	});
};

/**
 * List of Articles
 */
exports.requestedConnections = function(req, res) {
	var entity = req.user;
	var entityId = req.query.entityId;
	Connection.find({$and: [{connected: false}, {from: entityId}]}).sort('-created').populate('from', 'displayName').populate('to', 'displayName').exec(function(err, users) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(users);
		}
	});
};

/**
 * Article middleware
 */
exports.entityByID = function(req, res, next, id) {

	if (!mongoose.Types.ObjectId.isValid(id)) {
		return res.status(400).send({
			message: 'Entity is invalid'
		});
	}

	User.findById(id).exec(function(err, user) {
		if (err) return next(err);
		if (!user) {
			return res.status(404).send({
				message: 'Entity not found'
			});
		}
		req.user = user;
		next();
	});
};

/**
 * Article authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.user.id !== req.user.id) {
		return res.status(403).send({
			message: 'User is not authorized'
		});
	}
	next();
};
