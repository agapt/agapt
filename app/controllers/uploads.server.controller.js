'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Upload = mongoose.model('Upload'),
	fs = require('fs'),
	mkdirp = require('mkdirp'),
	_ = require('lodash');

/**
 * Create an upload record and save the uploaded file
 */
exports.create = function(req, res) {
	var file = req.files.file;
	var offeringId = req.body.offeringId;

	mkdirp('public/data/uploads/', function (err) {
	    if (err) {
	    	console.log('#### the error: ', err);
	    }
	    else {
	    	fs.readFile(file.path, function (err, data) {
			  var fileName = offeringId + '-' + file.originalFilename;
			  var publicPath = 'data/uploads/' + fileName;
			  var newPath = 'public/' + publicPath;
			  fs.writeFile(newPath, data, function (err) {
			  	if(err){
			  		return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
			  	}
			    var upload = new Upload({
					fileName: fileName,
					originalFileName: file.originalFilename,
					size: file.size,
					type: file.type,
					path: publicPath,
					user: req.user,
					owner: offeringId
				});

				upload.save(function(err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(upload);
					}
				});
			  });
			});
		}
	});
};

exports.read = function(req, res) {
	console.log('the uploads 2: ', req.uploads);
	res.json({'images': req.uploads});
};

exports.uploadByID = function(req, res, next, id) {

	if (!mongoose.Types.ObjectId.isValid(id)) {
		return res.status(400).send({
			message: 'Upload is invalid'
		});
	}

	Upload.find({owner : id}).sort('-created').populate('user', 'displayName').exec(function(err, uploads) {
		if (err) return next(err);
		console.log('the uploads: ', uploads);
		req.uploads = uploads;
		next();
	});
};