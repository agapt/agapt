'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	deepPopulate = require('mongoose-deep-populate'),
	Schema = mongoose.Schema;

/**
 * Produce Schema
 */
var ConsumeSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	entity: {
		type: Schema.ObjectId,
		ref: 'Entity'
	},
	item: {
		type: Schema.ObjectId,
		ref: 'Item'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Consume', ConsumeSchema);
ConsumeSchema.plugin(deepPopulate);
