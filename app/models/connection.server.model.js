'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Connection Schema
 */
var ConnectionSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	from: {
		type: Schema.ObjectId,
		ref: 'Entity'
	},
	to: {
		type: Schema.ObjectId,
		ref: 'Entity'
	},
	connected:{
		type: Boolean,
		default: false
	}
});

mongoose.model('Connection', ConnectionSchema);
