'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	//elmongo = require('elmongo'),
	mongoosastic = require('mongoosastic'),
	Schema = mongoose.Schema;

/**
 * Article Schema
 */
var ItemSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	name: {
		type: String,
		default: '',
		trim: true,
		required: 'Name cannot be blank'
	},
	summary: {
		type: String,
		default: '',
		trim: true
	},
	entity: {
		type: Schema.ObjectId,
		ref: 'Entity'
	},
	description: {
		type: String,
		default: '',
		trim: true
	},
	bullets: {
		type: Array,
		default: []
	},
	keywords: {
		type: Array,
		default: []
	},
	media: [{
		type: Schema.ObjectId,
		ref: 'Upload'
	}],
	producing: {
		type: [{
			type: Schema.ObjectId,
			ref: 'Entity'
		}],
		default: []
	},
	consuming: {
		type: [{
			type: Schema.ObjectId,
			ref: 'Entity'
		}],
		default: []
	},
	price: {
		type: Number,
		default: 0
	},
	units: {
		type: Number,
		default: 0
	},
	rating: {
		type: Number,
		default: 5
	},
	Published: {
		type: Boolean,
		default: true
	},
	type: {
		type: [{
			type: String,
			enum: ['product', 'service']
		}]
	}
});

//ItemSchema.plugin(elmongo);
ItemSchema.plugin(mongoosastic);

mongoose.model('Item', ItemSchema);

var Item = mongoose.model('Item');

var stream = Item.synchronize();
var count = 0;
stream.on('data', function(err, doc){
console.log('### the doc name', doc, err);
  count++;
});
stream.on('close', function(){
  console.log('indexed ' + count + ' documents!');
});
stream.on('error', function(err){
  console.log(err);
});
/*Item.sync(function (err, numSynced) {
  // all items are now searchable in elasticsearch
  console.log('##### error in sync: ', err, numSynced);
});*/
