'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	//elmongo = require('elmongo'), 
	Schema = mongoose.Schema;

/**
 * Article Schema
 */
var LoginAsSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: String,
		default: '',
		trim: true,
		required: 'User cannot be blank'
	},
	password: {
		type: String,
		default: '',
		trim: true
	}
});


mongoose.model('LoginAs', LoginAsSchema);