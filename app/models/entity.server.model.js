'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Article Schema
 */
var EntitySchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	name: {
		type: String,
		trim: true,
		default: ''
	},
	summary: {
		type: String,
		trim: true,
		default: ''
	},
	description: {
		type: String,
		trim: true,
		default: ''
	},
	location: {
		type: String,
		trim: true,
		default: ''
	},
	address: {
		type: String,
		trim: true,
		default: ''
	},
	thumbnail: {
		type: String,
		trim: true,
		default: ''
	},
	media: {
		type: String,
		trim: true,
		default: ''
	},
	rating: {
		type: String,
		trim: true,
		default: ''
	},
	usage_plan: {
		type: String,
		trim: true,
		default: ''
	},
	messages: [{
		content: {
			type: String,
			default: '',
			trim: true
		},
		from: {
			type: Schema.ObjectId,
			ref: 'User'
		},
		created: {
			type: Date,
			default: Date.now
		}
	}]
});

mongoose.model('Entity', EntitySchema);
