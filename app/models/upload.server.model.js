'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Article Schema
 */
var UploadSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	fileName: {
		type: String,
		default: '',
		trim: true,
		required: 'File name cannot be blank'
	},
	originalFileName: {
		type: String,
		default: '',
		trim: true,
		required: 'Original file name cannot be blank'
	},
	size: {
		type: Number
	},
	type: {
		type: String,
		default: '',
		trim: true
	},
	path: {
		type: String,
		default: '',
		trim: true,
		required: 'Path cannot be blank'
	},
	owner: {
		type: Schema.ObjectId,
		ref: 'Item'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Upload', UploadSchema);
