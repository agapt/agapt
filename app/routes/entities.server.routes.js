'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	entities = require('../../app/controllers/entities.server.controller');

module.exports = function(app) {
	// Article Routes
	app.route('/entities')
		.get(entities.list);

	app.route('/entities/:entityId')
		.get(entities.read);

	// Finish by binding the article middleware
	app.param('entityId', entities.entityByID);

	app.route('/connections/suggest')
		.get(entities.suggestConnections);

	app.route('/connections')
		.get(entities.listConnections)
		.post(users.requiresLogin, entities.connect);

	app.route('/connections/:connId')
		.delete(users.requiresLogin, entities.deleteConnection);

	app.route('/connections/pending')
		.get(entities.pendingConnections);

	app.route('/connections/pending/:connId')
		.put(users.requiresLogin, entities.update);
		//.delete(users.requiresLogin, articles.hasAuthorization, articles.delete);

	app.route('/connections/requested')
		.get(entities.requestedConnections);
};
