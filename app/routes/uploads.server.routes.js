'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	uploads = require('../../app/controllers/uploads.server.controller');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function(app) {
	// Article Routes
	app.route('/uploads')
		.post(multipartMiddleware, uploads.create);

	app.route('/uploads/:uploadId')
		.get(uploads.read);

	// Finish by binding the article middleware
	app.param('uploadId', uploads.uploadByID);
};
