'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	consumes = require('../../app/controllers/consumes.server.controller');

module.exports = function(app) {
	// Consume Routes
	app.route('/consumers')
		.get(consumes.consumers);

	app.route('/consumes')
		.get(consumes.consumed)
		.post(users.requiresLogin, consumes.create);
};
