'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	elasticEearch = require('../../app/controllers/elastic-search.server.controller');

module.exports = function(app) {
	app.route('/elastic-search')
		.get(elasticEearch.search);
};
