'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	bulkUploads = require('../../app/controllers/bulk-uploads.server.controller');

module.exports = function(app) {
	// Article Routes
	app.route('/bulk-uploads')
		.post(bulkUploads.create);
};
