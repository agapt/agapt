'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	offerings = require('../../app/controllers/offerings.server.controller');

module.exports = function(app) {
	// Offering Routes
	app.route('/offerings')
		.get(offerings.list)
		.post(users.requiresLogin, offerings.create);

	app.route('/offerings/:offeringId')
		.get(offerings.read)
		.put(users.requiresLogin, offerings.update)
		.delete(users.requiresLogin, offerings.delete);

	// Finish by binding the offering middleware
	app.param('offeringId', offerings.offeringByID);
};
