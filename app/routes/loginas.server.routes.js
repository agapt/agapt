'use strict';

/**
 * Module dependencies.
 */

module.exports = function(app) {
	// User Routes
	var loginas = require('../../app/controllers/loginas.server.controller');

	// Setting up the users profile api
	app.route('/loginas/')
		.get(loginas.list)
		.post(loginas.create);
};
