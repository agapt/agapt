'use strict';

module.exports = {
	app: {
		title: 'MEAN.JS',
		description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
		keywords: 'mongodb, express, angularjs, node.js, mongoose, passport'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	// The secret should be set to a non-guessable string that
	// is used to compute a session hash
	sessionSecret: 'MEAN',
	// The name of the MongoDB collection to store sessions in
	sessionCollection: 'sessions',
	// The session cookie settings
	sessionCookie: {
		path: '/',
		httpOnly: true,
		// If secure is set to true then it will cause the cookie to be set
		// only when SSL-enabled (HTTPS) is used, and otherwise it won't
		// set a cookie. 'true' is recommended yet it requires the above
		// mentioned pre-requisite.
		secure: false,
		// Only set the maxAge to null if the cookie shouldn't be expired
		// at all. The cookie will expunge when the browser is closed.
		maxAge: null,
		// To set the cookie in a specific domain uncomment the following
		// setting:
		// domain: 'yourdomain.com'
	},
	// The session cookie name
	sessionName: 'connect.sid',
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'combined',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			stream: 'access.log'
		}
	},
	assets: {
		lib: {
			css: [
				//We certainly dont need this if we have the template css which is a collation of everything
				//'public/lib/bootstrap/dist/css/bootstrap.css',
				//'public/lib/bootstrap/dist/css/bootstrap-theme.css',

				//widget specific
				'public/lib/ng-tags-input/ng-tags-input.min.css'
			],
			js: [
				//'public/lib/angular/angular.js',
				//'public/lib/angular-resource/angular-resource.js',
				//'public/lib/angular-animate/angular-animate.js',
				//'public/lib/angular-ui-router/release/angular-ui-router.js',
				//'public/lib/angular-ui-utils/ui-utils.js',
				//'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				
				//common libraries. required for every page
				//include jquery BEFORE angular so $(el).html() may resolve scripts. see http://stackoverflow.com/a/12200540/1298418
				'public/lib/jquery/dist/jquery.min.js',
				'public/lib/angular/angular.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/ngstorage/ngStorage.min.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-ui-utils-event/event.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'public/lib/bootstrap-select/dist/js/bootstrap-select.min.js',
				'public/lib/angular-bootstrap-select/build/angular-bootstrap-select.min.js',

				// page specific angular libs. may be removed
				'public/lib/datatables/media/js/jquery.dataTables.js',
				'public/lib/angular-datatables/dist/angular-datatables.min.js',
				'public/lib/angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.min.js',
				'public/lib/angular-ui-calendar/src/calendar.js',


				// common libs. previous bootstrap-sass version was used, but due to a need to have single compiled file using bootstrap's version
				'public/lib/bootstrap/dist/js/bootstrap.min.js',
				'public/lib/slimScroll/jquery.slimscroll.min.js',
				'public/lib/widgster/widgster.js',
				'public/lib/jquery-touchswipe/jquery.touchSwipe.min.js',
				// ENd of template sing  -->

				//widget specific
				'public/lib/angular-file-upload/angular-file-upload.min.js',
				'public/lib/ng-tags-input/ng-tags-input.min.js'
			]
		},
		css: [
			//Ignoring for now
			//'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
